
package com.thrarin.learnmorsecode;

import android.app.Application;
import android.content.Context;

import com.thrarin.learnmorsecode.util.MorseCodePreferences;

public class App extends Application {

    private static Context sContext;
    private static MorseCodePreferences sPreferences;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        sPreferences = MorseCodePreferences.getPreferenceInstace();
    }

    public static Context getContext() {
        return sContext;
    }

    public static MorseCodePreferences getPreferenceUtils() {
        return sPreferences;
    }

}
