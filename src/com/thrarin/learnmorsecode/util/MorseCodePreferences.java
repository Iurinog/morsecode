
package com.thrarin.learnmorsecode.util;

import com.thrarin.learnmorsecode.App;

import android.content.Context;
import android.content.SharedPreferences;

public class MorseCodePreferences {

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    private static MorseCodePreferences sMorseCodePreferences;

    /**
     * Unique instance
     * 
     * @return
     */
    public static MorseCodePreferences getPreferenceInstace() {
        if (sMorseCodePreferences == null) {
            sMorseCodePreferences = new MorseCodePreferences();
        }

        return sMorseCodePreferences;
    }

    private MorseCodePreferences() {
        sharedPref = App.getContext().getSharedPreferences(Constants.SHARED_PREFENRECE_NAME,
                Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public boolean writeString(String key, String value) {
        editor.putString(key, value);
        return editor.commit();
    }

    public boolean writeInteger(String key, int value) {
        editor.putInt(key, value);
        return editor.commit();
    }

    public String getStringValue(String key) {
        return sharedPref.getString(key, "");
    }

    public int getIntValue(String key, int defaultValue) {
        return sharedPref.getInt(key, defaultValue);
    }

}
