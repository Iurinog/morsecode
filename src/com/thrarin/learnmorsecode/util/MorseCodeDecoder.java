
package com.thrarin.learnmorsecode.util;

import java.util.List;

public class MorseCodeDecoder {

    private static final String LINE_SYMBOL = "2";
    private static final String POINT_SYMBOL = "1";

    public static int getCodeSymbol(long time) {

        if (time >= Constants.LONG_CLICK_CONSIDERED) {
            return 2;
        } else {
            return 1;
        }
    }

    public static String[] getCodeSymbol(char character) {

        int value = getCharacterCode(character);
        String symbols = String.valueOf(value);

        String[] codeSymbol = new String[symbols.length()];

        for (int i = 0; i < symbols.length(); i++) {
            codeSymbol[i] = String.valueOf(symbols.charAt(i));
        }

        return codeSymbol;
    }

    public static Character getCodeCharacter(List<Long> codes) {

        StringBuilder codeSequence = new StringBuilder();

        for (Long code : codes) {

            if (code >= Constants.LONG_CLICK_CONSIDERED) {
                codeSequence.append(LINE_SYMBOL);
            } else {
                codeSequence.append(POINT_SYMBOL);
            }
        }

        return getCharacter(codeSequence.toString());
    }

    private static Character getCharacter(String codeSequence) {

        int value = 0;

        try {
            value = Integer.valueOf(codeSequence);
        } catch (NumberFormatException ex) {
            // TODO Implementing method comparing by string
        }

        switch (value) {

        // Alphabet
            case 12:
                return 'A';
            case 2111:
                return 'B';
            case 2121:
                return 'C';
            case 211:
                return 'D';
            case 1:
                return 'E';
            case 1121:
                return 'F';
            case 221:
                return 'G';
            case 1111:
                return 'H';
            case 11:
                return 'I';
            case 1222:
                return 'J';
            case 212:
                return 'K';
            case 1211:
                return 'L';
            case 22:
                return 'M';
            case 21:
                return 'N';
            case 222:
                return 'O';
            case 1221:
                return 'P';
            case 2212:
                return 'Q';
            case 121:
                return 'R';
            case 111:
                return 'S';
            case 2:
                return 'T';
            case 112:
                return 'U';
            case 1112:
                return 'V';
            case 122:
                return 'W';
            case 2112:
                return 'X';
            case 2122:
                return 'Y';
            case 2211:
                return 'Z';

            case 12212:
                return '�';
            case 1212:
                return '�';
            case 11211:
                return '�';
            case 22122:
                return '�';
            case 2221:
                return '�';
            case 1122:
                return '�';

                // Numbers
            case 12222:
                return '1';
            case 11222:
                return '2';
            case 11122:
                return '3';
            case 11112:
                return '4';
            case 11111:
                return '5';
            case 21111:
                return '6';
            case 22111:
                return '7';
            case 22211:
                return '8';
            case 22221:
                return '9';
            case 22222:
                return '0';

                // Others
            case 221122:
                return ',';
            case 121212:
                return '.';
            case 112211:
                return '?';
            case 212122:
                return '!';
            case 212121:
                return ';';
            case 222111:
                return ':';
            case 122221:
                return '\'';
            case 211112:
                return '-';
            case 21121:
                return '/';
            case 21221:
                return '(';
            case 212212:
                return ')';

            default:
                return ' ';
        }
    }

    private static int getCharacterCode(char character) {

        switch (character) {
            case 'A':
                return 12;
            case 'B':
                return 2111;
            case 'C':
                return 2121;
            case 'D':
                return 211;
            case 'E':
                return 1;
            case 'F':
                return 1121;
            case 'G':
                return 221;
            case 'H':
                return 1111;
            case 'I':
                return 11;
            case 'J':
                return 1222;
            case 'K':
                return 212;
            case 'L':
                return 1211;
            case 'M':
                return 22;
            case 'N':
                return 21;
            case 'O':
                return 222;
            case 'P':
                return 1221;
            case 'Q':
                return 2212;
            case 'R':
                return 121;
            case 'S':
                return 111;
            case 'T':
                return 2;
            case 'U':
                return 112;
            case 'V':
                return 1112;
            case 'W':
                return 122;
            case 'X':
                return 2112;
            case 'Y':
                return 2122;
            case 'Z':
                return 2211;

            case '1':
                return 12222;
            case '2':
                return 11222;
            case '3':
                return 11122;
            case '4':
                return 11112;
            case '5':
                return 11111;
            case '6':
                return 21111;
            case '7':
                return 22111;
            case '8':
                return 22211;
            case '9':
                return 22221;
            case '0':
                return 22222;

            case '�':
                return 12212;
            case '�':
                return 1212;
            case '�':
                return 11211;
            case '�':
                return 22122;
            case '�':
                return 2221;
            case '�':
                return 1122;

            case ',':
                return 221122;
            case '.':
                return 121212;
            case '?':
                return 112211;
            case '!':
                return 212122;
            case ';':
                return 212121;
            case ':':
                return 222111;
            case '\'':
                return 122221;
            case '-':
                return 211112;
            case '/':
                return 21121;
            case '(':
                return 21221;
            case ')':
                return 212212;

            default:
                return 0;
        }

    }

}
