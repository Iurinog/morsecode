
package com.thrarin.learnmorsecode.util;

public class Constants {

    /**
     * All variables are be using considering milliseconds
     */
    public static final int LONG_CLICK_CONSIDERED = 200;
    public static final int WORD_TIME_GAP = 1000;
    public static final int LETTER_TIME_GAP = 400;

    /* Shared preferences */
    public static final String SHARED_PREFENRECE_NAME = "morse_code";
    public static final String KEY_LETTER_GAP = "letter_gap";
    public static final String KEY_WORD_GAP = "word_gap";
    
    /* Max number of codes of a letter */
    public static final int MAX_LETTER_CODES = 6;
    
    /* Key between activities */
    public static final String BUNDLE_MORSE_CODE = "entire_morse_code";
}
