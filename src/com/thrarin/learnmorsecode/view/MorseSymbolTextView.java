/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.thrarin.learnmorsecode.view;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.thrarin.learnmorsecode.R;
import com.thrarin.learnmorsecode.util.Constants;

public class MorseSymbolTextView extends TextView {

    private SpannableStringBuilder mSpannableString = null;
    private String mText = "";
    private int mMaxLength = -1;

    private static final int TEXT_DRAWABLE_CIRCLE_IDS[] = new int[] {
            R.drawable.morse_point, R.drawable.morse_line, R.drawable.morse_space
    };

    public MorseSymbolTextView(Context context) {
        this(context, null);
    }

    public MorseSymbolTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MorseSymbolTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CharSequence text = "";

        mMaxLength = Constants.MAX_LETTER_CODES;
        mSpannableString = new SpannableStringBuilder();
        setLineSpacing(0, 10);
        
        if (text.length() > 0) {
            setText(text);
        }
    }

    @Override
    public void setText(CharSequence text, TextView.BufferType type) {
        mText = "";
        if (mSpannableString != null) {
            mSpannableString.clear();
        }

        if (text == null || mSpannableString == null) {
            super.setText(text, type);
            return;
        }

        if (mMaxLength > -1 && text.length() > mMaxLength) {
            text = text.subSequence(0, mMaxLength);
        }

        for (int i = 0; i < text.length(); i++) {
            insertImageSpan(mSpannableString, text.charAt(i));
        }

        mText = text.toString();
        super.setText(mSpannableString, BufferType.SPANNABLE);
    }

    @Override
    public void append(CharSequence text, int start, int end) {
        if (text == null || mSpannableString == null
                || (mMaxLength > -1 && mText.length() == mMaxLength)) {
            //super.append(text, start, end);
            return;
        }

        for (int i = 0; i < text.length(); i++) {
            insertImageSpan(mSpannableString, text.charAt(i));
        }
        mText = mText + text.toString();
        super.setText(mSpannableString, BufferType.SPANNABLE);
    }

    @Override
    public CharSequence getText() {
        return mText;
    }

    @Override
    public int length() {
        return mText.length();
    }
    
    public void setMaxSize(int value) {
        this.mMaxLength = value;
    }

    private void insertImageSpan(SpannableStringBuilder spannableString, char digit) {
        int len = spannableString.length();
        spannableString.append("\uFFFC");
        ImageSpan imageSpan = getImageSpan(digit);
        if (imageSpan != null) {
            spannableString.setSpan(imageSpan, len, spannableString.length(),
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
    }

    private ImageSpan getImageSpan(char digit) {
        ImageSpan imageSpan = null;
        int textDrawable[] = TEXT_DRAWABLE_CIRCLE_IDS;

        int index = 0;
        try {
            index = Integer.parseInt(digit + "");
        } catch (NumberFormatException e) {
        }
        if (index > 0 && index <= textDrawable.length) {
            imageSpan = new ImageSpan(getContext(), textDrawable[index - 1]);
        }
        return imageSpan;
    }

}
