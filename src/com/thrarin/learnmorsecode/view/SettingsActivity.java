
package com.thrarin.learnmorsecode.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

import com.thrarin.learnmorsecode.App;
import com.thrarin.learnmorsecode.R;
import com.thrarin.learnmorsecode.util.Constants;

public class SettingsActivity extends Activity {

    private Spinner mLetterSpinner;
    private Spinner mWordSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        mLetterSpinner = (Spinner) findViewById(R.id.letterTimeSpinner);
        mWordSpinner = (Spinner) findViewById(R.id.wordTimeSpinner);

        // mLetterSpinner.setOnItemSelectedListener(mOnLetterSpinnerItemSelectedListener);
        // mWordSpinner.setOnItemSelectedListener(mOnWordSpinnerItemSelectedListener);

        mLetterSpinner.setSelection(getSpinnerValue(mLetterSpinner, App.getPreferenceUtils()
                .getIntValue(Constants.KEY_LETTER_GAP, Constants.LETTER_TIME_GAP)));
        mWordSpinner.setSelection(getSpinnerValue(mWordSpinner, App.getPreferenceUtils()
                .getIntValue(Constants.KEY_WORD_GAP, Constants.WORD_TIME_GAP)));
    }

    private AdapterView.OnItemSelectedListener mOnLetterSpinnerItemSelectedListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            String itemSelected = mLetterSpinner.getItemAtPosition(position).toString();
            itemSelected = itemSelected.replace(" ms", "");
            int value = Integer.valueOf(itemSelected);
            App.getPreferenceUtils().writeInteger(Constants.KEY_LETTER_GAP, value);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener mOnWordSpinnerItemSelectedListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            String itemSelected = mWordSpinner.getItemAtPosition(position).toString();
            itemSelected = itemSelected.replace(" ms", "");
            int value = Integer.valueOf(itemSelected);
            App.getPreferenceUtils().writeInteger(Constants.KEY_WORD_GAP, value);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private int getSpinnerValue(Spinner spinner, int time) {
        int index = 0;
        int size = spinner.getCount();
        for (int i = 0; i < size; i++) {
            if (spinner.getItemAtPosition(i).toString().contains(String.valueOf(time))) {
                index = i;
            }
        }

        return index;
    }

    private int getSpinnerValue(Spinner spinner, String time, int defaultValue) {
        int index = 0;
        int size = spinner.getCount();
        for (int i = 0; i < size; i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(time)) {

                String itemSelected = spinner.getItemAtPosition(i).toString();
                itemSelected = itemSelected.replace(" ms", "");
                int value = Integer.valueOf(itemSelected);

                return value;
            }
        }

        return defaultValue;
    }

    @Override
    protected void onPause() {

        App.getPreferenceUtils().writeInteger(
                Constants.KEY_LETTER_GAP,
                getSpinnerValue(mLetterSpinner,
                        mLetterSpinner.getSelectedItem().toString(), Constants.LETTER_TIME_GAP));

        App.getPreferenceUtils().writeInteger(
                Constants.KEY_WORD_GAP,
                getSpinnerValue(mWordSpinner,
                        mWordSpinner.getSelectedItem().toString(), Constants.WORD_TIME_GAP));

        super.onPause();
    }
}
