
package com.thrarin.learnmorsecode.view;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thrarin.learnmorsecode.App;
import com.thrarin.learnmorsecode.R;
import com.thrarin.learnmorsecode.model.Code;
import com.thrarin.learnmorsecode.util.Constants;
import com.thrarin.learnmorsecode.util.MorseCodeDecoder;

public class MainActivity extends Activity {

    private ImageView mMorseCodeView;
    private TextView mMorseCodeTextView;
    private TextView mMorseCodeInstructionsTextView;
    private MorseSymbolTextView mMorseSymbolTextView;

    private ImageView mMorseCodeClean;
    private ImageView mMorseCodeSee;
    private ImageView mMorseCodeShareFace;
    private ImageView mMorseCodeShareTwitter;
    private ImageView mMorseCodeShareGPlus;

    // Variables used to calculate time between the touch's
    private long mTimeWhenActionDown = 0;
    private long mTimeWhenActionUp = 0;

    // Variable when user try to touch more than the max available
    private boolean isListenningTouches = false;

    private Code mCurrentCode;
    private List<Code> mMorseCode;

    private StringBuilder mMorseCodeText;
    private int mLetterGapTime = 0;
    private int mWordGapTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMorseCodeView = (ImageView) findViewById(R.id.morse_machine);
        mMorseCodeView.setOnTouchListener(mOnMorseCodeViewTouchListener);

        mMorseCodeTextView = (TextView) findViewById(R.id.morse_text_view);
        mMorseCodeInstructionsTextView = (TextView) findViewById(R.id.text_instructions);
        mMorseSymbolTextView = (MorseSymbolTextView) findViewById(R.id.morse_text_symbol);

        mMorseCodeClean = (ImageView) findViewById(R.id.morse_button_clean);
        mMorseCodeClean.setOnClickListener(mOnCleanButtonClickListener);
        mMorseCodeSee = (ImageView) findViewById(R.id.morse_see_code);
        mMorseCodeSee.setOnClickListener(mOnCleanButtonClickListener);
        mMorseCodeShareFace = (ImageView) findViewById(R.id.morse_share_facebook);
        mMorseCodeShareFace.setOnClickListener(mOnCleanButtonClickListener);
        mMorseCodeShareTwitter = (ImageView) findViewById(R.id.morse_share_twitter);
        mMorseCodeShareTwitter.setOnClickListener(mOnCleanButtonClickListener);
        mMorseCodeShareGPlus = (ImageView) findViewById(R.id.morse_share_gplus);
        mMorseCodeShareGPlus.setOnClickListener(mOnCleanButtonClickListener);

        mMorseCode = new ArrayList<Code>();
        mCurrentCode = new Code();
        mMorseCodeText = new StringBuilder();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLetterGapTime = App.getPreferenceUtils().getIntValue(Constants.KEY_LETTER_GAP,
                Constants.LETTER_TIME_GAP);
        mWordGapTime = App.getPreferenceUtils().getIntValue(Constants.KEY_WORD_GAP,
                Constants.WORD_TIME_GAP);

        isListenningTouches = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        mLetterGapTime = App.getPreferenceUtils().getIntValue(Constants.KEY_LETTER_GAP,
                Constants.LETTER_TIME_GAP);
        mWordGapTime = App.getPreferenceUtils().getIntValue(Constants.KEY_WORD_GAP,
                Constants.WORD_TIME_GAP);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivityForResult(intent, 0);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private View.OnClickListener mOnCleanButtonClickListener = new OnClickListener() {

        @Override
        public void onClick(View v) {

            int viewId = v.getId();

            switch (viewId) {
                case R.id.morse_button_clean:

                    mMorseCodeInstructionsTextView.setText(R.string.txt_start);
                    mMorseCodeInstructionsTextView.setTextColor(Color.BLACK);
                    mMorseSymbolTextView.setText("");
                    mMorseCodeTextView.setText("");

                    mMorseCode = new ArrayList<Code>();
                    mCurrentCode = new Code();
                    mMorseCodeText = new StringBuilder();

                    mTimeWhenActionDown = 0;
                    mTimeWhenActionUp = 0;

                    isListenningTouches = false;

                    break;
                case R.id.morse_see_code:

                    Intent intent = new Intent(MainActivity.this, MorseCodeActivity.class);
                    intent.putExtra(Constants.BUNDLE_MORSE_CODE, mMorseCodeTextView.getText());
                    startActivity(intent);
                    break;
                case R.id.morse_share_facebook:
                    Toast.makeText(App.getContext(), R.string.btn_share_face_fail,
                            Toast.LENGTH_SHORT).show();
                    break;
                case R.id.morse_share_twitter:
                    Toast.makeText(App.getContext(), R.string.btn_share_twitter_fail,
                            Toast.LENGTH_SHORT).show();
                    break;
                case R.id.morse_share_gplus:
                    Toast.makeText(App.getContext(), R.string.btn_share_gplus_fail,
                            Toast.LENGTH_SHORT).show();
                    break;
                default:
                    break;
            }

        }
    };

    private View.OnTouchListener mOnMorseCodeViewTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {

            int action = event.getAction();

            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    mTimeWhenActionDown = event.getDownTime();

                    long gapTime = mTimeWhenActionDown - mTimeWhenActionUp;

                    if (!isListenningTouches && gapTime > mLetterGapTime) {
                        isListenningTouches = true;
                        mMorseCodeInstructionsTextView.setText(R.string.txt_instructions);
                        mMorseCodeInstructionsTextView.setTextColor(Color.BLACK);
                    }

                    if (isListenningTouches) {
                        // Check if it's space
                        if (gapTime >= mWordGapTime) {
                            mMorseCode.add(mCurrentCode);

                            // Update Views
                            mMorseCodeText.append("  "); // Add space
                            mMorseCodeTextView.setText(mMorseCodeText);
                            mMorseSymbolTextView.setText("");

                            mCurrentCode = new Code();
                        } else if (gapTime >= mLetterGapTime) {
                            mMorseCode.add(mCurrentCode);

                            // Update View
                            mMorseCodeText.append(mCurrentCode.getCodeChar());
                            mMorseCodeTextView.setText(mMorseCodeText);
                            mMorseSymbolTextView.setText("");

                            mCurrentCode = new Code();
                        }
                    }
                    break;
                case MotionEvent.ACTION_UP:

                    mTimeWhenActionUp = event.getEventTime();
                    long pressedTime = (mTimeWhenActionUp - mTimeWhenActionDown);

                    if (isListenningTouches) {

                        mCurrentCode.addCode(pressedTime);

                        int textSize = mMorseCodeText.length();

                        mMorseCodeText.replace(textSize - 1, textSize, mCurrentCode.getCodeChar()
                                .toString());
                        mMorseCodeTextView.setText(mMorseCodeText);

                        mMorseSymbolTextView.append(String.valueOf(MorseCodeDecoder
                                .getCodeSymbol(pressedTime)));

                        if (mCurrentCode.getSize() > Constants.MAX_LETTER_CODES) {
                            mMorseCodeInstructionsTextView.setText(R.string.txt_max_codes);
                            mMorseCodeInstructionsTextView.setTextColor(Color.RED);
                            isListenningTouches = false;
                        }
                    }

                    break;
                default:
                    break;
            }
            return true;
        }
    };

}
