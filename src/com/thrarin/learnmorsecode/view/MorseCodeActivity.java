
package com.thrarin.learnmorsecode.view;

import android.app.Activity;
import android.os.Bundle;

import com.thrarin.learnmorsecode.R;
import com.thrarin.learnmorsecode.util.Constants;
import com.thrarin.learnmorsecode.util.MorseCodeDecoder;

public class MorseCodeActivity extends Activity {

    private MorseSymbolTextView mMorseSymbolTextView;
    private String mMorseCodeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_morse_code);

        mMorseSymbolTextView = (MorseSymbolTextView) findViewById(R.id.morse_symbol_all);
        mMorseSymbolTextView.setMaxSize(200);

        mMorseCodeText = getIntent().getStringExtra(Constants.BUNDLE_MORSE_CODE);

        createSymbols();
    }

    private void createSymbols() {

        int phraseSize = mMorseCodeText.length();
        mMorseCodeText = mMorseCodeText.toString();

        for (int i = 0; i < phraseSize; i++) {
            char letter = mMorseCodeText.charAt(i);
            
            if (letter == ' ') {
                mMorseSymbolTextView.append(String.valueOf(3));
            } else {
             
                String[] symbol = MorseCodeDecoder.getCodeSymbol(letter);

                for (int j = 0; j < symbol.length; j++) {
                    mMorseSymbolTextView.append(symbol[j]);
                }
                
            }
        }

    }

}
