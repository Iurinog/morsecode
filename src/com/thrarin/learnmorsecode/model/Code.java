
package com.thrarin.learnmorsecode.model;

import java.util.ArrayList;
import java.util.List;

import com.thrarin.learnmorsecode.util.MorseCodeDecoder;

public class Code {

    private List<Long> mCodes;

    /**
     * Constructor
     */
    public Code() {
        mCodes = new ArrayList<Long>();
    }

    public boolean addCode(long code) {
        return mCodes.add(code);
    }

    public int getSize() {
        return mCodes.size();
    }
    
    /**
     * Get respective Character
     * 
     * @return {@link Character} according touch sequence
     */
    public Character getCodeChar() {
        return MorseCodeDecoder.getCodeCharacter(this.mCodes);
    }

}
